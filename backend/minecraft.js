'use strict';

const fs = require('fs'),
    path = require('path'),
    readline = require('readline'),
    { spawn } = require('child_process');

module.exports = exports = {
    status,
    start,
    stop,
    command,
    addLogListener,
    removeLogListener
};

let gMinecraft = null, gLogHistory = [];
const gLogListeners = new Set();

function getMemoryLimitMbSync() {
    let ram;
    if (fs.existsSync('/sys/fs/cgroup/cgroup.controllers')) { // cgroup v2
        const data = fs.readFileSync('/sys/fs/cgroup/memory.max', 'utf8');
        if (data.trim() === 'max') ram = 4 * 1024 * 1024 * 1024; // "max" means unlimited
        else ram = parseInt(data.trim(), 10);
    } else {
        const data = fs.readFileSync('/sys/fs/cgroup/memory/memory.limit_in_bytes', 'utf8'); // this is the RAM. we have equal amount of swap
        ram = parseInt(data.trim(), 10);
    }

    return Math.round(ram / (1024 * 1024));
}

function status() {
    return { running: !!gMinecraft };
}

function emitLine(line) {
    switch (process.env.MC_FLAVOR) {
    case 'java-edition': line = line.replace(/\[.*\] \[.*\]: /, ''); break;
    case 'forge': line = line.replace(/\[.*\] \[.*] \[.*\]: /, ''); break;
    case 'bedrock': line = line.replace(/NO LOG FILE! - \[.*\] /, ''); break;
    }

    for (let l of gLogListeners) {
        l.emit('line', line);
    }
}

async function start() {
    const memoryLimitMB = getMemoryLimitMbSync();

    if (process.env.MC_FLAVOR === 'forge') {
        const opts = { cwd: '/app/data' };
        emitLine(`Starting Forge Server with memory limit ${memoryLimitMB}M`);
        // run.sh is missing in latest releases - https://forums.minecraftforge.net/topic/140090-missing-runsh-runbat/
        // gMinecraft = spawn('java', [`-Xmx${memoryLimitMB}M`, `-Xms${memoryLimitMB}M`, `@/app/code/forge/libraries/net/minecraftforge/forge/${process.env.MC_VERSION}-${process.env.FORGE_VERSION}/unix_args.txt`, 'nogui', '"$@"'], opts);
        gMinecraft = spawn('java', [`-Xmx${memoryLimitMB}M`, `-Xms${memoryLimitMB}M`, '-jar', path.join(__dirname, `../forge/forge-${process.env.MC_VERSION}-${process.env.FORGE_VERSION}-shim.jar`), 'nogui'], opts);
    } else if (process.env.MC_FLAVOR === 'java-edition') {
        const opts = { cwd: '/app/data' };
        emitLine(`Starting Java Server with memory limit ${memoryLimitMB}M`);
        gMinecraft = spawn('java', [`-Xmx${memoryLimitMB}M`, `-Xms${memoryLimitMB}M`, '-jar', path.join(__dirname, '../minecraft_server.jar'), 'nogui'], opts);
    } else if (process.env.MC_FLAVOR === 'bedrock') {
        emitLine('Starting Bedrock Server');
        const opts = { cwd: '/app/code', env: { LD_LIBRARY_PATH: '/app/code' } };
        gMinecraft = spawn(path.join(opts.cwd, 'bedrock_server'), [], opts);
    }

    const logLineStream = readline.createInterface({
        input: gMinecraft.stdout
    });
    logLineStream.on('line', function (line) {
        console.log(line.toString()); // also log to stdout
        gLogHistory.push(line);
        if (gLogHistory.length > 20) gLogHistory.shift();
        emitLine(line);
    });

    gMinecraft.stderr.pipe(process.stderr);
    // minecraft.stdout.pipe(process.stdout);
    // process.stdin.pipe(minecraft.stdin);

    gMinecraft.on('close', function () {
        gMinecraft = null;
    });
}

async function stop() {
    emitLine('Stopping minecraft server');

    if (!gMinecraft) return;

    return new Promise((resolve) => {
        let killTimerId = null;
        gMinecraft.on('close', function () {
            gMinecraft = null;
            if (killTimerId) clearTimeout(killTimerId);
            emitLine('Stopped minecraft server');
            resolve();
        });

        gMinecraft.kill('SIGTERM');
        killTimerId = setTimeout(() => { if (gMinecraft) gMinecraft.kill('SIGKILL'); }, 5000);
    });
}

async function command(cmd) {
    if (!gMinecraft) return;
    emitLine(`Executing command: ${cmd}`);
    gMinecraft.stdin.write(cmd + '\n');
}

function addLogListener(socket) {
    gLogListeners.add(socket);
    for (const line of gLogHistory) emitLine(line); // push history
}

function removeLogListener(socket) {
    gLogListeners.delete(socket);
}
