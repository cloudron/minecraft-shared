'use strict';

const assert = require('assert'),
    crypto = require('crypto'),
    express = require('express'),
    fs = require('fs'),
    http = require('http'),
    { HttpSuccess, HttpError } = require('connect-lastmile'),
    lastMile = require('connect-lastmile'),
    oidc = require('express-openid-connect'),
    minecraft = require('./minecraft.js'),
    path = require('path'),
    safe = require('safetydance'),
    socketIO = require('socket.io'),
    util = require('util');

module.exports = exports = {
    start
};

async function healthcheck(req, res) {
    res.status(200).send();
}

function oidcLogin(req, res) {
    res.oidc.login({
        returnTo: '/',
        authorizationParams: {
            redirect_uri: process.env.CLOUDRON_APP_ORIGIN + '/api/oidc/callback',
        },
    });
}

function isAuthenticated (req, res, next) {
    if (!req.oidc.isAuthenticated()) return next(new HttpError(401, 'Unauthorized'));

    next();
}

function getProfile(req, res, next) {
    const user = req.oidc.user;

    next(new HttpSuccess(200, { user }));
}

async function startMinecraft(req, res, next) {
    const [error] = await safe(minecraft.start());
    if (error) return next(new HttpError(409, 'minecraft could not be started'));

    next(new HttpSuccess(202, {}));
}

async function stopMinecraft(req, res, next) {
    const [error] = await safe(minecraft.stop());
    if (error) return next(new HttpError(409, 'minecraft could not be stopped'));

    next(new HttpSuccess(202, {}));
}

function status(req, res, next) {
    next(new HttpSuccess(200, { status: minecraft.status() }));
}

async function runCommand(req, res, next) {
    const [error] = await safe(minecraft.command(req.body.cmd));
    if (error) return next(new HttpError(409, 'minecraft not running'));

    next(new HttpSuccess(202, {}));
}

async function getFlavor(req, res) {
    res.status(200).send({ flavor: process.env.MC_FLAVOR });
}

async function start(port) {
    assert.strictEqual(typeof port, 'number');

    const router = express.Router();
    router.del = router.delete;

    const app = express();
    const httpServer = http.Server(app);
    const io = socketIO(httpServer);

    router.get ('/api/healthcheck', healthcheck);
    router.get ('/api/flavor', getFlavor);

    router.get ('/api/oidc/login', oidcLogin);
    router.get ('/api/profile', isAuthenticated, getProfile);

    router.get ('/api/status', isAuthenticated, status);
    router.post('/api/start', isAuthenticated, startMinecraft);
    router.post('/api/stop', isAuthenticated, stopMinecraft);
    router.post('/api/command', isAuthenticated, runCommand);

    let sessionSecret = safe.fs.readFileSync('/app/data/session.secret', 'utf8');
    if (!sessionSecret) {
        sessionSecret = crypto.randomBytes(20).toString('hex');
        fs.writeFileSync('/app/data/session.secret', sessionSecret);
    }

    const oidcAuth = oidc.auth({
        // CLOUDRON_OIDC_PROVIDER_NAME is not supported
        issuerBaseURL: process.env.CLOUDRON_OIDC_ISSUER,
        baseURL: process.env.CLOUDRON_APP_ORIGIN,
        clientID: process.env.CLOUDRON_OIDC_CLIENT_ID,
        clientSecret: process.env.CLOUDRON_OIDC_CLIENT_SECRET,
        secret:  sessionSecret,
        authorizationParams: {
            response_type: 'code',
            scope: 'openid profile email'
        },
        authRequired: false,
        routes: {
            callback: '/api/oidc/callback',
            login: false,
            logout: '/api/oidc/logout'
        }
    });

    app.use(oidcAuth);
    app.use(express.json());
    app.use(router);
    app.use('/', express.static(path.resolve(__dirname, '../frontend')));
    app.use(lastMile());

    // socketio middleware. only allow authenticated users (https://github.com/auth0/express-openid-connect/pull/152#issuecomment-719682819)
    io.use((socket, next) => {
        const req = socket.request;
        const res = new http.ServerResponse(req);
        res.assignSocket(socket);

        res.on('finish', () => res.socket.destroy());
        app(req, res, function (error) {
            if (error) return next(error);
            if (!req.oidc.isAuthenticated()) return next(new HttpError(401, 'Unauthorized'));

            next();
        });
    });

    io.on('connection', function (socket) {
        console.log(`New IO connection with socket ${socket.id}`);
        socket.on('cmd', function (cmd) {
            minecraft.cmd(cmd);
        });
        minecraft.addLogListener(socket);

        socket.on('disconnect', function (reason) {
            console.log(`Socket ${socket.id} disconnected: ${reason}`);
            minecraft.removeLogListener(socket);
        });
    });

    await util.promisify(httpServer.listen.bind(httpServer))(port, '0.0.0.0');
}
