'use strict';

/* global Vue */
/* global window */
/* global io */
/* global document */
/* global superagent */

new Vue({
    el: '#app',
    data: {
        status: {},
        logstream: [],
        cmd: '',
        flavor: 'unknown',
        profile: null,
        inited: false,
        refreshTimerId: null,
        startStopBusy: false
    },
    methods: {
        onError: function (error) {
            console.log(error);
        },
        refresh: function (callback) {
            superagent.get('/api/status').end((error, result) => {
                if (error) return this.onError(error);
                if (result.statusCode !== 200) return this.onError('Unexpected response: ' + result.statusCode + ' ' + result.text);

                this.status = result.body.status;
                if (callback) callback();
            });
        },
        onLogin: function () {
            window.location.href = '/api/oidc/login';
        },
        onLogout: function () {
            clearTimeout(this.refreshTimerId);
            this.refreshTimerId = null;
            window.location.href = '/api/oidc/logout';
        },
        start: function () {
            this.startStopBusy = true;
            superagent.post('/api/start').end((error, result) => {
                if (error) return this.onError(error);
                if (result.statusCode !== 202) return this.onError('Unexpected response: ' + result.statusCode + ' ' + result.text);
                this.refresh(() => this.startStopBusy = false);
            });
        },
        stop: function () {
            this.startStopBusy = true;
            superagent.post('/api/stop').end((error, result) => {
                this.startStopBusy = false;
                if (error) return this.onError(error);
                if (result.statusCode !== 202) return this.onError('Unexpected response: ' + result.statusCode + ' ' + result.text);
                this.refresh(() => this.startStopBusy = false);
            });
        },
        sendCommand: function () {
            superagent.post('/api/command').send({ cmd: this.cmd }).end((error, result) => {
                if (error) return this.onError(error);
                if (result.statusCode !== 202) return this.onError('Unexpected response: ' + result.statusCode + ' ' + result.text);

                this.cmd = '';
            });
        },
        loadLogs: function () {
            const socket = io();
            socket.on('line', (line) => {
                this.logstream.push(line);

                Vue.nextTick(function () {
                    var elem = document.getElementsByClassName('logstream')[0];
                    elem.scrollTop = elem.scrollHeight - 100;
                });
            });
        }
    },
    mounted: function () {
        this.inited = false;

        superagent.get('/api/flavor').end((error, result) => {
            this.flavor = result.body.flavor;
            switch (this.flavor) {
            case 'forge': document.title = 'Minecraft Forge Server'; break;
            case 'bedrock': document.title = 'Minecraft Bedrock Server'; break;
            case 'java-edition': document.title = 'Minecraft Server (Java Edition)'; break;
            }

            superagent.get('/api/profile').end((error, result) => {
                this.inited = true;

                if (result && result.statusCode === 401) return;
                if (error) return this.onError(error);

                this.profile = result.body.user;
                this.refreshTimerId = setInterval(this.refresh.bind(this), 5000);
                this.refresh();
                this.loadLogs();
            });
        });
    }
});
