#!/usr/bin/env node

'use strict';

const minecraft = require('./backend/minecraft.js'),
    server = require('./backend/server.js');

const PORT = process.env.PORT || 3000;

(async function () {
    await server.start(parseInt(PORT));
    console.log(`Server is up and running on port ${PORT}`);
    await minecraft.start();
})();
